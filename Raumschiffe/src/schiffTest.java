
public class schiffTest {

	public static void main(String[] args) {
		// Klassen instanziieren		
		Raumschiff klingonen = new Raumschiff("IKS Hengh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);
		
		Ladung klingonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonen2 = new Ladung("Bat'leth Klingnen Schwert", 200);
		klingonen.addLadung(klingonen1);
		klingonen.addLadung(klingonen2);
		
		Ladung romulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung romulaner2 = new Ladung("Rote Materie", 2);
		Ladung romulaner3 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(romulaner1);
		romulaner.addLadung(romulaner2);
		romulaner.addLadung(romulaner3);
		
		Ladung vulkanier1 = new Ladung("Forschungssonde", 35);		
		Ladung vulkanier2 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(vulkanier1);
		vulkanier.addLadung(vulkanier2);
		
		// Kampf
		
		klingonen.photonentorpedosAbschiessen(romulaner);
		space();
		romulaner.phaserkanonenAbschiessen(klingonen);
		space();
		vulkanier.nachrichtAnAlleSenden("Gewalt ist nicht logisch.");
		space();
		klingonen.zustandAusgeben();
		space();
		klingonen.ladungenAuslesen();
		space();
		vulkanier.reparaturAndroidSenden(true, true, true, 5);
		space();
		vulkanier.torpedorohrLaden(3);
		space();
		vulkanier.ladungAufraeumen();
		space();
		klingonen.photonentorpedosAbschiessen(romulaner);
		space();
		klingonen.photonentorpedosAbschiessen(romulaner);
		space();
		klingonen.zustandAusgeben();
		klingonen.ladungenAuslesen();		
		space();
		romulaner.zustandAusgeben();
		romulaner.ladungenAuslesen();
		space();
		vulkanier.zustandAusgeben();
		vulkanier.ladungenAuslesen();
		space();
		
			
	}
	/**
	 * Separator Methode.
	 */
	public static void space() {
		System.out.println("+++++++++++++++++++++++++++++++");
	}

}

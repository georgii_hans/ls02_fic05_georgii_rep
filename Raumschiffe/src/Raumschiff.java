import java.util.ArrayList;

/**
 * Modellierung eines Raumschiffes
 * @author Hans Georgii
 * @version 1.0 */
public class Raumschiff {

	// Attribute
	private String name;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungssysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> laderaum = new ArrayList<Ladung>();
	
	// Methoden
	/** Klasse Raumschiff - Konstruktor ohne Parameter */
	public Raumschiff() {
		
	}
	
	/**
	 * Klasse Raumschiff - Konstruktor mit Parameter
	 * @param name 						-> Name des Raumschiffes (String)
	 * @param energieversorgung 		-> Energieversorgung Prozent (int)
	 * @param schutzschilde 			-> Schutzschilde Prozent (int)
	 * @param lebenserhaltungssysteme 	-> Zustand des Lebensunterhaltungssystemes Prozent (int)
	 * @param huelle 					-> Zustand der Huelle Prozent (int)
	 * @param photonentorpedos 			-> Anzahl Photonentorpedos (int)
	 * @param reparaturAndroiden 		-> Anzahl ReparaturAndroiden (int) */
	public Raumschiff(String name, int energieversorgung, int schutzschilde, int lebenserhaltungssysteme, int huelle, int photonentorpedos, int reparaturAndroiden) {
		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reparaturAndroiden = reparaturAndroiden;	
	}
	
	// Beginn getter und setter Methoden
	/** 
	 * Name einsetzen
	 * @param name -> der Name des Raumschiffes (String)
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Gibt den Name des Raumschiffes zur�ck
	 * @return name -> der Name des Raumschiffes (String)
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Energieversogung einsetzen
	 * @param energieversorgung -> Energieversorgung Prozent (int)
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	/**
	 * Gibt Energieversorgung zur�ck
	 * @return energieversorgung -> Energieversorgung Prozent (int)
	 */
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	
	/**
	 * Schutzschilde einsetzen
	 * @param schutzschilde -> Schutzschilde Prozent (int)
	 */
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	/**
	 * Gibt Schutzschilde zur�ck
	 * @return name -> Schutzschilde Prozent (int)
	 */
	public int getSchutzschilde() {
		return this.schutzschilde;
	}
	
	/**
	 * Lebenserhaltungssysteme einsetzen
	 * @param lebenserhaltungssysteme -> Lebenserhaltungssysteme Prozent (int)
	 */
	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	/**
	 * Gibt Lebenserhaltungssysteme zur�ck
	 * @return lebenserhaltungssysteme -> Lebenserhaltungssysteme Prozent (int)
	 */
	public int getLebenserhaltungssysteme() {
		return this.lebenserhaltungssysteme;
	}
	
	/**
	 * Huelle einsetzen
	 * @param huelle -> Huelle Prozent (int)
	 */
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}
	/**
	 * Gibt Huelle zur�ck
	 * @return huelle -> Huelle Prozent (int)
	 */
	public int getHuelle() {
		return this.huelle;
	}
	
	/**
	 * Photonentorpedos einsetzen
	 * @param photonentorpedos -> Photonentorpedos Anzahl (int)
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	/**
	 * Gibt Energieversorgung zur�ck
	 * @return photonentorpedos -> Photonentorpedos Anzahl (int)
	 */
	public int getPhotonentorpedos() {
		return this.photonentorpedos;
	}

	/**
	 * Reparaturandroiden einsetzen
	 * @param reparaturAndroiden -> Reparaturandroiden Anzahl (int)
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	/**
	 * Gibt Reparaturandroiden zur�ck
	 * @return reparaturAndroiden -> Reparaturandroiden Anzahl (int)
	 */
	public int getReparaturAndroiden() {
		return this.reparaturAndroiden;
	}
	// Ende getter und setter Methoden
	
	// Begin zusaetzliche Methode
	/**
	 * F�gt ein Objekt der Klasse Ladung zu der Array Liste laderaum
	 * @see Ladung.java
	 * @param neueLadung -> Objekt der Klasse Ladung
	 */
	public void addLadung(Ladung neueLadung) {
		laderaum.add(neueLadung);
	}	
	
	/**
	 * Gibt Art und Menge von jeder Ladung Objekt in der Array Laderaum aus
	 */
	public void ladungenAuslesen() {
		System.out.println("Ladung des Raumschiffes " + getName() + ":");
		this.laderaum.forEach((laden) -> System.out.println(laden.getArt() + ": " + laden.getMenge()));
	}
	
	/**
	 * Laderaum aufr�umen: l�scht jedes Ladungsobjekt aus der Array Laderaum mit Menge 0.
	 */
	public void ladungAufraeumen() {
		System.out.println("Ladung des Raumschiffes " + getName() + " aufger�umt.");
		this.laderaum.removeIf((laden) -> (laden.getMenge() <= 0));
	}
	
	/**
	 * Wenn ein Ladung von Photonentorpedos ist da, l�dt eine Bestimmte Menge davon in 
	 * laderaum.
	 * Wenn kein Photonentorpedo vorhanden ist, gibt "Keine Photonentorpedos gefunden!"
	 * aus.
	 * 
	 * @param torpedosZuLaden -> Anzahl von Torpedos hochzuladen (int)
	 */
	public void torpedorohrLaden(int torpedosZuLaden) {		
		int ladungsMenge;
		String ladungsArt = "Photonentorpedo";
		
		for (Ladung tempLadung : laderaum) {
	        if (ladungsArt.equals(tempLadung.getArt())) {
	        	ladungsMenge = tempLadung.getMenge();	
	        	
	        	if (ladungsMenge <= 0) {
	    			System.out.println(this.name + " : " + "Keine Photonentorpedos gefunden!");
	    			nachrichtAnAlleSenden("-=*Click*=-");
	    			return;
	    		} else if (torpedosZuLaden > ladungsMenge) { 	    			
	    			tempLadung.setMenge(0);
	    			this.photonentorpedos += ladungsMenge;
	    			System.out.println(this.name + " : " + ladungsMenge + " Photonentorpedo(s) eingesetzt");
	    			return;
	    		} else if (torpedosZuLaden <= ladungsMenge) {
	    			tempLadung.setMenge(ladungsMenge - torpedosZuLaden);
	    			this.photonentorpedos += torpedosZuLaden;
	    			System.out.println(this.name + " : " + torpedosZuLaden + " Photonentorpedo(s) eingesetzt");
	    			return;
	    		}	
	        	
	        	System.out.println("Keine Photonentorpedos gefunden!");
	    		nachrichtAnAlleSenden("-=*Click*=-");
	    	}
	    }	        	    	        
	}		
	
	/**
	 * Gibt der Zustand des Raumchiffes aus: Energieversorgung, Schutzschilde
	 * Lebenserhaltungssysteme, Huelle, Photonentorpedos, ReparaturAndroiden.
	 */
	public void zustandAusgeben() {
		System.out.println("Zustand des Raumschiffes: " + this.getName());
		System.out.println("Energieversorgung: " + this.getEnergieversorgung() + "%");
		System.out.println("Schutzschilde: " + this.getSchutzschilde() + "%");
		System.out.println("Lebenserhaltungssysteme: " + this.getLebenserhaltungssysteme() + "%");																
		System.out.println("Huelle: " + this.getHuelle() + "%");
		System.out.println("Photonentorpedos: " + this.getPhotonentorpedos());
		System.out.println("ReparaturAndroiden: " + this.getReparaturAndroiden());
	}
	
	// Kampfmethoden
	/**
	 * Schie�t einen Torpedo ab, dann vermerkt eine Treffer. 
	 * Wenn keinen Torpedo vorhanden ist, gibt eine Nachricht aus.
	 * @see trefferVermerken
	 * @see nachrichtAnAlleSenden
	 * @param ziel -> Raumschiff Objekt als eingabe Parameter
	 */
	public void photonentorpedosAbschiessen(Raumschiff ziel) {
		if (photonentorpedos == 0) {
			nachrichtAnAlleSenden("-=*Click*=-");		
		} else { 
			photonentorpedos--;
			nachrichtAnAlleSenden("Photonentorpedo abgeschossen.");
			trefferVermerken(ziel);
		}
	}
	
	/**
	 * Schie�t den Phaserkanonen ab, dann vermerkt eine Treffer. 
	 * Wenn nicht genugend Energie vorhanden ist, gibt eine Nachricht aus.
	 * @see trefferVermerken
	 * @see nachrichtAnAlleSenden	 * 
	 * @param ziel -> Raumschiff Objekt als eingabe Parameter
	 */
	public void phaserkanonenAbschiessen (Raumschiff ziel) {
		if (energieversorgung < 50) {
			nachrichtAnAlleSenden("-=*Click*=-");
		} else {
			energieversorgung -= 50;
			nachrichtAnAlleSenden("Phaserkanonen abgeschossen.");
			trefferVermerken(ziel);
		}			
	}	
	
	/**
	 * Methode f�r Treffer an Raumschiffe zu vermerken.
	 * Bekommt als param ein Raumschiffsobjekt und reduziert die entsprechende 
	 * Attribute.
	 * Schutzschilde - 50
	 * If Schutzschilde <= 0, dann Huelle - 50 und Energieversorgung - 50
	 * If Huelle <= 0, dann Lebensunterhaltungssysteme = 0 und Nachricht aussenden
	 * @see nachrichtAnAlleSenden
	 * @param schiff -> Raumschiff Objekt als eingabe Parameter
	 */
	private void trefferVermerken (Raumschiff schiff) {
		System.out.println(schiff.getName() + " wurde getroffen!");
		schiff.setSchutzschilde(schiff.getSchutzschilde() - 50);		
		
		if (schiff.getSchutzschilde() <= 0) {
			schiff.setHuelle(schiff.getHuelle() - 50);
			schiff.setEnergieversorgung(schiff.getEnergieversorgung() - 50);
		}

		if (schiff.getHuelle() <= 0) {
			schiff.setLebenserhaltungssysteme(0);
			schiff.nachrichtAnAlleSenden("Lebenserhaltssysteme des Raumschiffes " + schiff.getName() + " vernichtet.");
		}
	}
	
	/**
	 * Eine Nachricht an Alle senden.
	 * F�gt ein String zu Array broadcastKommunikator hinzu und dann gibt die Nachricht aus.
	 * @param nachricht -> Nachricht im Kommunikator zu speichern (String)
	 */
	public void nachrichtAnAlleSenden (String nachricht) {
		broadcastKommunikator.add(this.name + " : " + nachricht);
		System.out.println(this.name + " : " + nachricht);
	}
	
	/**
	 * Gibt alle Eintr�ge im Broadcastkommunikator Array aus.
	 */
	public void logbuchAuslesen() {
		this.broadcastKommunikator.forEach((log) -> System.out.println(log));
	}
	
	/**
	 * Sendet Reparaturandroiden den Schiff zu reparieren. Sie erh�en die eingegebene
	 * Params um ein Random Wert.
	 * @param huelleReparieren -> Ob Huelle repariert werden soll (Boolean)
	 * @param schutzschildeReparieren -> Ob Schutzschilde repariert werden soll (Boolean)
	 * @param energieversorgungReparieren -> Ob Energieversorgung repariert werden soll (Boolean)
	 * @param androidenZuSenden -> Anzahl von Androiden zu senden
	 */
	public void reparaturAndroidSenden(boolean huelleReparieren, boolean schutzschildeReparieren, boolean energieversorgungReparieren, int androidenZuSenden) {
		int androidenVorhanden = 0;
		int anzahlStrukturen = 0;
		double zufallszahl = Math.random() * 100;
		
		if (huelleReparieren == true)             anzahlStrukturen++;
		if (schutzschildeReparieren == true)      anzahlStrukturen++;	
		if (energieversorgungReparieren == true)  anzahlStrukturen++;
				
		if (this.reparaturAndroiden <= 0) {
			System.out.println("Keine Androiden vorhanden");			
		} else if (androidenZuSenden >= this.reparaturAndroiden) {
			androidenVorhanden += this.reparaturAndroiden;
			System.out.println(this.name +": reparatur Androiden wurden gesendet.");
		} else if (androidenZuSenden < this.reparaturAndroiden) {
			androidenVorhanden = androidenZuSenden;
			System.out.println(this.name +": reparatur Androiden wurden gesendet.");
		}
		
		double berechnung = (zufallszahl * androidenVorhanden)/anzahlStrukturen;
		
		if (huelleReparieren == true)             this.huelle += berechnung;
		if (schutzschildeReparieren == true)      this.schutzschilde += berechnung;	
		if (energieversorgungReparieren == true)  this.energieversorgung += berechnung;
	}

}
/**
 * Modellierung einer Ladung
 * @author Hans Georgii
 * @version 1.0 */
public class Ladung {
	
	// Attribute
	private String art;
	private int menge;
	
	// Methode
	/** Klasse Ladung - Konstruktor ohne Parameter */
	public Ladung () {
		
	}
	
	/**
	 * Klasse Ladung - Konstruktor mit Parameter
	 * @param art   -> Art der Ladung
	 * @param menge -> Ladungsmenge
	 */
	public Ladung (String art, int menge) {
		this.art = art;
		this.menge = menge;
	}
	
	//Getter und Setter
	/**
	 * Gibt den Art der Ladung zur�ck
	 * @return art -> Art der Ladung (String)
	 */
	public String getArt () {
		return this.art;
	}
	/**
	 * Art der Ladung einsetzen
	 * @param art -> Art der Ladung (String)
	 */
	public void setArt (String art) {
		this.art = art;
	}
	
	/**
	 * Gibt die Ladungsmenge zur�ck
	 * @return menge -> Ladungsmenge (int)
	 */
	public int getMenge () {
		return this.menge;
	}
	/**
	 * Ladungsmenge einsetzen
	 * @param menge -> Ladungsmenge (int)
	 */
	public void setMenge (int menge) {
		this.menge = menge;
	}
}
